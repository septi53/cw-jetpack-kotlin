/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.diceware

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.commonsware.jetpack.diceware.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModel()

  private val openDoc = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
    motor.generatePassphrase(it)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    motor.results.observe(this) { viewState ->
      when (viewState) {
        MainViewState.Loading -> {
          binding.progress.visibility = View.VISIBLE
          binding.passphrase.text = ""
        }
        is MainViewState.Content -> {
          binding.progress.visibility = View.GONE
          binding.passphrase.text = viewState.passphrase
        }
        is MainViewState.Error -> {
          binding.progress.visibility = View.GONE
          binding.passphrase.text = viewState.throwable.localizedMessage
          Log.e(
            "Diceware",
            "Exception generating passphrase",
            viewState.throwable
          )
        }
      }
    }
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.open -> {
        openDoc.launch(arrayOf("text/plain"))
        return true
      }

      R.id.refresh -> {
        motor.generatePassphrase()
        return true
      }

      R.id.word_count_4, R.id.word_count_5, R.id.word_count_6, R.id.word_count_7,
      R.id.word_count_8, R.id.word_count_9, R.id.word_count_10 -> {
        item.isChecked = !item.isChecked
        motor.generatePassphrase(Integer.parseInt(item.title.toString()))

        return true
      }
    }

    return super.onOptionsItemSelected(item)
  }
}
