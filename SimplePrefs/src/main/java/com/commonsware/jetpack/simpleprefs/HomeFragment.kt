/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.simpleprefs

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.commonsware.jetpack.sampler.util.ViewBindingFragment
import com.commonsware.jetpack.simpleprefs.databinding.FragmentHomeBinding

class HomeFragment :
  ViewBindingFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    val motor: HomeMotor by viewModels()

    useBinding { binding ->
      binding.state = motor.states

      binding.edit.setOnClickListener {
        NavHostFragment.findNavController(this@HomeFragment)
          .navigate(R.id.editPrefs)
      }
    }
  }
}
